{
    'name': 'Payment Term Due Amount Installment',
    'version': '1.0',
    'author': 'Avision Infosoft',
    'category': 'Accounting',
    'description': """
        Module allows to analyse all payment due installment besed on the payment term in customer invoice and vendor bill.
    """,
    'price': 30,
    'currency': 'EUR',
    'summary': 'Module allows to analyse all payment due installment besed on the payment term in customer invoice and vendor bill.',
    'depends': ['base', 'account','account_invoicing'],
    'data': [
        'security/ir.model.access.csv',
        'views/account_invoice.xml'
    ],
    'images': ['static/description/main_screenshot.png'],
    'installable': True,
    'auto_install': False,
}