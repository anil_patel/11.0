from odoo import api, models,fields
import odoo.addons.decimal_precision as dp
# from odoo.tools import format_date
from datetime import datetime, timedelta,date
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from dateutil.relativedelta import relativedelta
import calendar

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    pt_installments_ids = fields.One2many('payment.term.instalmment.line','invoice_id', compute='_compute_installments', store=True)

    @api.model
    @api.depends('amount_total','payment_term_id','date_invoice')
    def _compute_installments(self):
        pass
        line_lst = []
        amount = 0
        if self.payment_term_id:
            for each in self.payment_term_id.line_ids:
                if each.value == 'percent':
                    amount_per = (self.amount_total * each.value_amount) / 100
                    amount += amount_per
                    if each.option == 'day_after_invoice_date':
                        date_inv = False
                        if self.date_invoice:
                            date_inv = datetime.strptime(self.date_invoice, DF) + timedelta(days=each.days)
                        else:
                            date_inv = date.today() + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Percent' + '(' + str(each.value_amount) + '% of ' + str(self.amount_total) + ')'
                        }))
                    if each.option == 'fix_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF).replace(day=1) + relativedelta(
                                months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        else:
                            date_next_inv = date.today().replace(day=1) + relativedelta(months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Percent' + '(' + str(each.value_amount) + '% of ' + str(
                                self.amount_total) + ')'
                        }))
                    if each.option == 'last_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF) + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today() + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Percent' + '(' + str(each.value_amount) + '% of ' + str(
                                self.amount_total) + ')'

                        }))
                    if each.option == 'last_day_current_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today()
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Percent' + '(' + str(each.value_amount) + '% of ' + str(
                                self.amount_total) + ')'

                        }))

                if each.value == 'fixed':
                    amount_per = each.value_amount
                    amount += amount_per
                    if each.option == 'day_after_invoice_date':
                        date_inv = False
                        if self.date_invoice:
                            date_inv = datetime.strptime(self.date_invoice, DF) + timedelta(days=each.days)
                        else:
                            date_inv = date.today() + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Fixed' + '(' + str(each.value_amount) + ' of ' + str(
                                self.amount_total) + ')'
                        }))
                    if each.option == 'fix_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF).replace(day=1) + relativedelta(
                                months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        else:
                            date_next_inv = date.today().replace(day=1) + relativedelta(months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Fixed' + '(' + str(each.value_amount) + ' of ' + str(
                                self.amount_total) + ')'
                        }))
                    if each.option == 'last_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF) + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today() + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Fixed' + '(' + str(each.value_amount) + ' of ' + str(
                                self.amount_total) + ')'
                        }))
                    if each.option == 'last_day_current_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today()
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount': amount_per,
                            'due_date': date_inv,
                            'amount_type': 'Fixed' + '(' + str(each.value_amount) + ' of ' + str(
                                self.amount_total) + ')'
                        }))
                if each.value == 'balance':
                    amount_per = each.value_amount
                    amount += amount_per
                    if each.option == 'day_after_invoice_date':
                        date_inv = False
                        if self.date_invoice:
                            date_inv = datetime.strptime(self.date_invoice, DF) + timedelta(days=each.days)
                        else:
                            date_inv = date.today() + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount':  self.amount_total - amount,
                            'due_date': date_inv,
                            'amount_type': 'Remaining Balance'
                        }))
                    if each.option == 'fix_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF).replace(day=1) + relativedelta(
                                months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        else:
                            date_next_inv = date.today().replace(day=1) + relativedelta(months=1)
                            date_inv = date_next_inv + timedelta(days=each.days)
                        line_lst.append((0, 0, {
                            'amount':  self.amount_total - amount,
                            'due_date': date_inv,
                            'amount_type': 'Remaining Balance'
                        }))
                    if each.option == 'last_day_following_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF) + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today() + relativedelta(months=1)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount':  self.amount_total - amount,
                            'due_date': date_inv,
                            'amount_type': 'Remaining Balance'
                        }))
                    if each.option == 'last_day_current_month':
                        date_inv = False
                        if self.date_invoice:
                            date_next_inv = datetime.strptime(self.date_invoice, DF)
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        else:
                            date_next_inv = date.today()
                            last_day_of_month = calendar.monthrange(date_next_inv.year, date_next_inv.month)[1]
                            date_inv = date_next_inv.replace(day=last_day_of_month)
                        line_lst.append((0, 0, {
                            'amount':  self.amount_total - amount,
                            'due_date': date_inv,
                            'amount_type': 'Remaining Balance'
                        }))
        self.pt_installments_ids = line_lst



class PaymentTermMilestonLine(models.Model):
    _name = 'payment.term.instalmment.line'

    invoice_id = fields.Many2one('account.invoice', string="Purchase Order")
    currency_id = fields.Many2one('res.currency', related="invoice_id.currency_id")
    amount = amount_total_signed = fields.Monetary(string='Amount', currency_field='currency_id',
        store=True, readonly=True)
    amount_type = fields.Char(string="Value")
    due_date = fields.Date(string="Due Date")